defmodule Triggerer.MixProject do
  use Mix.Project

  def project do
    [
      app: :triggerer,
      version: "1.0.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Triggerer.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.4.5"},
      {:distillery, "~> 1.5", runtime: false},
      {:cowboy, "~> 1.1"}
    ]
  end
end
