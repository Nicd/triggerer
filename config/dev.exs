use Mix.Config

secret = "dev.secret.exs"

if File.exists?(Path.join([__DIR__, secret])) do
  import_config(secret)
end
