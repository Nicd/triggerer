defmodule Triggerer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    port =
      case System.get_env("PORT") do
        nil -> 6000
        port when is_binary(port) -> String.to_integer(port)
      end

    # List all child processes to be supervised
    children = [
      Plug.Adapters.Cowboy.child_spec(
        :http,
        Triggerer.Router,
        [],
        port: port
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Triggerer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
