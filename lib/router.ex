defmodule Triggerer.Router do
  @moduledoc """
  Router dispatching requests to the correct handlers.
  """

  use Plug.Router
  use Plug.ErrorHandler
  require Logger

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  paths =
    quote do
      Triggerer.get_envs()
    end

  match "/:path" do
    case Map.get(unquote(paths), path) do
      nil ->
        route_not_found(conn)

      script ->
        case Triggerer.run(script) do
          :ok ->
            send_resp(conn, 200, "OK")

          {:error, :file_not_found} ->
            send_resp(conn, 404, "File not found.")

          error ->
            Logger.error("Unknown error: #{inspect(error)}")
            send_resp(conn, 500, "Unknown error.")
        end
    end
  end

  match _ do
    route_not_found(conn)
  end

  defp route_not_found(conn), do: send_resp(conn, 404, "Route not found.")

  defp handle_errors(conn, data) do
    Logger.error(inspect(data))
    send_resp(conn, conn.status, "Something went wrong.")
  end
end
